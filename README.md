
# Description du Projet

Dans ce projet, nous allons créer une application Todo complète avec toutes les fonctionnalités. Nous effectuerons toutes les opérations CRUD. Nous utiliserons React.js et, pour gérer nos états, nous utiliserons Redux. De plus, nous apprendrons à créer des animations simples en utilisant Framer Motion. J'espère que cela vous plaira.

## Ce que nous allonsutiliser
- React
- React Redux
- Framer Motion
- Icônes React
- React Hot Toast
- Et bien plus encore...

## Exigences
- Connaissance de base de ReactJs
- Connaissance de base de HTML, CSS
- **Fichiers de démarrage:** Vous pouvez trouver tous les fichiers de démarrage dans la branche `starter-files`. Vous pouvez accéder à la branche `starter-files`, télécharger les fichiers de démarrage au format zip, ou cloner le projet et exécuter `git checkout starter-files`.

## Pour Commencer
La manière recommandée de commencer le projet est de suivre le tutoriel YouTube. Vous y trouverez tous les guides étape par étape. Ou vous pouvez démarrer le projet par vous-même en suivant le guide ci-dessous.

Après avoir obtenu les fichiers de démarrage, vous devez vous rendre dans le répertoire du fichier et exécuter :

```bash
npm install
```

Et ensuite démarrer le serveur de développement :

```bash
npm run dev
```

## Outils Utilisés
- **Favicon:** Flaticon.com
- **Éditeur de Code:** VS Code

