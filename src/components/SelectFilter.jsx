import { useState } from "react";
import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { updateFilterStatus } from "../redux/todo/todoSlice";

export default function SelectFilter() {
  const initialFilter = useSelector((state)=>state.todo.filterStatus)
  console.log(initialFilter);

  const [filtre, setFiltre] = useState(initialFilter);
  const dispatch = useDispatch()

  const handleChange = (event) => {
    setFiltre(event.target.value);
    dispatch(updateFilterStatus(event.target.value))
  };

  

  return (
    <Box>
      <FormControl>
        <InputLabel id="syst-filtre">Filtre</InputLabel>
        <Select
          labelId="syst-filtre"
          value={filtre}
          label="Filtre"
          onChange={handleChange}
        >
          <MenuItem value="all">
            All
          </MenuItem>
          <MenuItem value="completed">Completed</MenuItem>
          <MenuItem value="no-completed">No-completed</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
