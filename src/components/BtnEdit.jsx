/* eslint-disable react/prop-types */
import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import EditIcon from "@mui/icons-material/Edit";
import { orange } from "@mui/material/colors";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Input } from "@mui/material";

import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { updateTodo } from "../redux/todo/todoSlice";
import { toast } from "react-toastify";

const TodoSchema = Yup.object().shape({
  nom: Yup.string()
    .required("Champ requis")
    .matches(/^\S/, "Le nom ne peut pas commencer par un espace")
    .matches(/\S/, "Le nom ne peut pas être composé uniquement d'espaces")
    .min(1, "Le nom ne peut pas être vide"),
});


const BtnEdit = ({ todoEdit })  => {
  const dispatch = useDispatch()
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const formik = useFormik({
    enableReinitialize : true,
    initialValues: {
      nom: todoEdit.nom || "",
    },
    validationSchema: TodoSchema,
    onSubmit: async (values) => {
      try {
        if (todoEdit.nom !== values.nom) {
          dispatch(updateTodo({
            id: todoEdit.id,
            nom: values.nom,
            status: todoEdit.status, 
          }));
          toast.success("Tâche modifiée avec succès", { autoClose: 1000 });
        } else {
          toast.error("Modification inutile , nom conforme", { autoClose: 800 });
        }
    
        formik.resetForm();
        setOpen(false);
      } catch (error) {
        console.error("Erreur lors de la modification de la tâche :", error);
      }
    },
    
  });

  const handleUpdate = () => {
    // alert( todoEdit)
  };
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "70%",
    bgcolor: "background.paper",
    border: "2px solid #F0F0F0",
    borderRadius: "20px",
    boxShadow: 24,
    p: 4,
  };
  return (
    <>
      <EditIcon
        sx={{ color: orange[500], fontSize: 30, cursor: "pointer" }}
        onClick={handleOpen}
        onKeyDown={handleUpdate}
      />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography id="transition-modal-title" variant="h6" component="h2">
              Edit task
            </Typography>
            <form onSubmit={formik.handleSubmit}>
              <Input
                sx={{ width: "100%", margin: "1rem 0" }}
                placeholder="Enter your task here ..."
                name="nom"
                onChange={formik.handleChange}
                value={formik.values.nom}
                onBlur={formik.handleBlur}
              />
              <div className="text-danger">
                {formik.touched.nom && formik.errors.nom ? (
                  <div>{formik.errors.nom}</div>
                ) : null}
              </div>

              <Button
                variant="contained"
                color="success"
                sx={{ marginRight: "1rem" }}
                type="submit"
                onClick={handleUpdate}
              >
                Modifier
              </Button>
              <Button variant="outlined" color="error" onClick={handleClose}>
                Cancel
              </Button>
            </form>
          </Box>
        </Fade>
      </Modal>
    </>
  );
};

export default BtnEdit;
