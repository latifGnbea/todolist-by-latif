import Grid from "@mui/material/Unstable_Grid2";
import ButtonAddTask from "./ButtonAddTask";
import SelectFilter from "./SelectFilter";

const AppHeader = () => {
  return (
    <>
      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{ display: "flex", justifyContent: "space-between" }}
      >
        <ButtonAddTask />

        <SelectFilter />
      </Grid>
      
    </>
  );
};

export default AppHeader;
