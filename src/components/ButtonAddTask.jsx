import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Input } from "@mui/material";

import { useFormik } from "formik";
import * as Yup from "yup";
import { v4 as uuidv4 } from "uuid";
import { useDispatch } from "react-redux";
import { addTodo } from "../redux/todo/todoSlice";

const TodoSchema = Yup.object().shape({
  nom: Yup.string()
    .required("Champ requis")
    .matches(/^\S/, "Le nom ne peut pas commencer par un espace")
    .matches(/\S/, "Le nom ne peut pas être composé uniquement d'espaces")
    .min(1, "Le nom ne peut pas être vide"),
});

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "70%",
  bgcolor: "background.paper",
  border: "2px solid #F0F0F0",
  borderRadius: "20px",
  boxShadow: 24,
  p: 4,
};

const ButtonAddTask = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      nom: "",
    },
    validationSchema: TodoSchema,
    onSubmit: async (values) => {
      try {
        const data = {
          ...values,
          status: false,
          time: new Date().toLocaleString(),
          id: uuidv4(),
        };

        // alert(JSON.stringify(data));
        dispatch(addTodo(data));

        formik.resetForm();
        setOpen(false);
      } catch (error) {
        console.error("Erreur lors de l'ajout de la tâche :", error);
      }
    },
  });
  return (
    <>
      <Button variant="contained" onClick={handleOpen}>
        Add task
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography id="transition-modal-title" variant="h6" component="h2">
              Add task
            </Typography>
            <form onSubmit={formik.handleSubmit}>
              <Input
                sx={{ width: "100%", margin: "1rem 0" }}
                placeholder="Enter your task here ..."
                name="nom"
                onChange={formik.handleChange}
                value={formik.values.nom}
                onBlur={formik.handleBlur}
              />
              <div className="text-danger">
                {formik.touched.nom && formik.errors.nom ? (
                  <div>{formik.errors.nom}</div>
                ) : null}
              </div>

              <Button
                variant="contained"
                color="success"
                sx={{ marginRight: "1rem" }}
                type="submit"
              >
                Envoyer
              </Button>
              <Button variant="outlined" color="error" onClick={handleClose}>
                Cancel
              </Button>
            </form>
          </Box>
        </Fade>
      </Modal>
    </>
  );
};

export default ButtonAddTask;
