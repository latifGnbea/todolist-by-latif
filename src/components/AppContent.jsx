import Grid from "@mui/material/Unstable_Grid2";
import TodoItem from "./TodoItem";
import { useSelector } from "react-redux";

// eslint-disable-next-line react/prop-types
const AppContent = () => {
  const todoList = useSelector((state) => state.todo.todoList);
  const filterTodo = useSelector((state) => state.todo.filterStatus);
  const sortedTodoList = [...todoList];

  const filteredTodoList = sortedTodoList.filter((item) => {
    if (filterTodo === "all") {
      return true;
    } else if (filterTodo === "completed") {
      return item.status === true;
    } else {
      return item.status === false;
    }
  });

  return (
    <Grid
      item
      xs={12}
      sm={12}
      md={12}
      sx={{ backgroundColor: "gray", borderRadius: "1rem", padding: "20px" }}
    >
      {filteredTodoList && filteredTodoList.length > 0 ? (
        filteredTodoList.map((todo) => <TodoItem key={todo.id} todo={todo} />)
      ) : (
        <p style={{ textAlign: "center", color: "white" }}>Veuillez ajouter vos taches</p>
      )}
    </Grid>
  );
};

export default AppContent;
