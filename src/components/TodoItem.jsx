/* eslint-disable react/no-unknown-property */
/* eslint-disable react/prop-types */
import Grid from "@mui/material/Grid";
import BtnDelete from "./BtnDelete";
import BtnEdit from "./BtnEdit";
import Checkbox from "./Checkbox";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { updateTodo } from "../redux/todo/todoSlice";
import { toast } from "react-toastify";

const TodoItem = ({ todo }) => {
  const dispatch = useDispatch();
  const [check, setCheck] = useState(todo.status);
  const styleChecked = {
    textDecorationLine: "line-through",
    color: "grey",
  };

  const handleCheckboxChange = () => {
    const updatedCheck = !check;
    setCheck(updatedCheck);

    dispatch(
      updateTodo({
        id: todo.id,
        nom: todo.nom,
        status: updatedCheck ? true : false,
      })
    );
  };

  return (
    <Grid
      container
      sx={{
        backgroundColor: "white",
        padding: "0.5rem",
        marginBottom: "0.5rem",
        borderRadius: "0.5rem",
        fontSize: "1.2rem",
      }}
    >
      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Grid sx={{ display: "flex", alignItems: "center" }}>
          <Checkbox check={check} setCheck={handleCheckboxChange} />

          <div style={check ? styleChecked : {}}>
            {todo.nom} <br /> <em>{todo.time}</em>
          </div>
        </Grid>
        <Grid>
          <BtnDelete idSuppr={todo.id} />
          {check ? "" : <BtnEdit todoEdit={todo} />}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TodoItem;
