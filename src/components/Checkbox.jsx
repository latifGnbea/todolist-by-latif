import { motion, useMotionValue, useTransform } from 'framer-motion';
import './svg.css'
const checkVariants = {
  initial: {
    color: '#fff',
  },
  checked: { pathLength: 1 },
  unchecked: { pathLength: 0 },
};

const boxVariants = {
  checked: {
    background: 'blue',
    transition: { duration: 0.1 },
  },
  unchecked: { background: 'grey', transition: { duration: 0.1 } },
};

function Checkbox({ check, setCheck }) {
  const pathLength = useMotionValue(0);
  const opacity = useTransform(pathLength, [0.05, 0.15], [0, 1]);

  const handleCheckboxChange = () => {
    setCheck(!check);
  };

  return (
    <motion.div
      animate={check ? 'checked' : 'unchecked'}
      className='svgBox'
      variants={boxVariants}
      onClick={handleCheckboxChange}
    >
      <motion.svg
        className='svgBox svg'
        viewBox="0 0 53 38"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <motion.path
          variants={checkVariants}
          animate={check ? 'checked' : 'unchecked'}
          style={{ pathLength, opacity }}
          fill="none"
          strokeMiterlimit="10"
          strokeWidth="6"
          d="M1.5 22L16 36.5L51.5 1"
          strokeLinejoin="round"
          strokeLinecap="round"
        />
      </motion.svg>
    </motion.div>
  );
}

export default Checkbox;

