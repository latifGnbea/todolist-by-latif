import { Typography } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";


const PageTitle = ({ children }) => {
  return (
    <Grid item xs={12}>
      <Typography
        variant="h3"
        component="h4"
        sx={{ textAlign: "center", color: "grey" }}
      >
        {children}
      </Typography>
    </Grid>
  );
};

export default PageTitle;
