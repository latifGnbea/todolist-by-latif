/* eslint-disable react/prop-types */
import DeleteIcon from "@mui/icons-material/Delete";
import { red } from "@mui/material/colors";

import * as React from "react";
import Popover from "@mui/material/Popover";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useDispatch } from "react-redux";
import { deleteTodo } from "../redux/todo/todoSlice";

const BtnDelete = ({idSuppr}) => {
    const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const handleDelete = () => {
    dispatch(deleteTodo(idSuppr))
  };


  return (
    <>
      <DeleteIcon
        sx={{
          color: red[500],
          fontSize: 30,
          marginRight: "0.3rem",
          cursor: "pointer",
        }}
        aria-describedby={id}
        onClick={handleClick}
        // onKeyDown={handleDelete}
      />

      {/* modal de suppression */}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <Typography sx={{ p: 2, textAlign: "center" }}>
          Confirmer la suppression. <br />
          <Button variant="outlined" color="error" onClick={handleDelete}>
            Supprimer
          </Button>
        </Typography>
      </Popover>
    </>
  );
};

export default BtnDelete;
