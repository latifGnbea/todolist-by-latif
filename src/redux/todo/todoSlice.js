/* eslint-disable no-unused-vars */
import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

const getInitialTodo = () => {
  const localTodoList = window.localStorage.getItem("todoList");
  if (localTodoList) {
    return JSON.parse(localTodoList);
  }
  window.localStorage.setItem("todoList", JSON.stringify([]));
  return [];
};

const initialValue = {
  filterStatus : 'all',
  todoList: getInitialTodo(),
};

export const todoSlice = createSlice({
  name: "todo",
  initialState: initialValue,
  reducers: {
    addTodo: (state, action) => {
      const updatedTodoList = [action.payload, ...state.todoList];

      try {
        const todoList = window.localStorage.getItem("todoList");
        const todoListArr = todoList ? JSON.parse(todoList) : [];

        // Vérifier si la tâche existe déjà dans la liste
        const isTaskDuplicate = todoListArr.some(
          (task) => task.nom === action.payload.nom
        );

        if (!isTaskDuplicate) {
          todoListArr.unshift({ ...action.payload });
          window.localStorage.setItem("todoList", JSON.stringify(todoListArr));
          state.todoList = updatedTodoList;
        } else {
          throw new Error("La tâche existe déjà dans la liste.");
        }
        // Afficher un toast d'erreur
        toast.success("Tache ajoutée avec success", { autoClose: 1000 });
      } catch (error) {
        console.error("Erreur lors de l'ajout de la tâche :", error);
        // Récupérer le message d'erreur pour l'afficher à l'utilisateur
        const errorMessage =
          error.message ||
          "Une erreur s'est produite lors de l'ajout de la tâche.";
        // Afficher un toast d'erreur
        toast.error(errorMessage, { autoClose: 1000 });
      }
    },
    deleteTodo: (state, action) => {
      const todoList = window.localStorage.getItem("todoList");

      if (todoList) {
        const todoListArr = JSON.parse(todoList);

        // Filtrer la liste pour obtenir une nouvelle liste sans la tâche à supprimer
        const updatedTodoListArr = todoListArr.filter(
          (todo) => todo.id !== action.payload
        );

        // Mettre à jour la liste dans localStorage
        window.localStorage.setItem(
          "todoList",
          JSON.stringify(updatedTodoListArr)
        );

        // Mettre à jour la liste dans l'état Redux
        state.todoList = updatedTodoListArr;
        toast.success("Tache supprimée avec success", { autoClose: 1000 });
      } else {
        toast.error("Erreur lors de la suppressin de la tache", {
          autoClose: 1000,
        });
      }
    },
    updateTodo: (state, action) => {
      const todoList = window.localStorage.getItem("todoList");

      if (todoList) {
        const todoListArr = JSON.parse(todoList);

        // Mettre à jour les propriétés de la tâche dans la liste
        todoListArr.forEach((todo, index) => {
          if (todo.id === action.payload.id) {
            todo.nom = action.payload.nom;
            todo.status = action.payload.status;
          }
        });

        // Mettre à jour la liste dans localStorage
        window.localStorage.setItem("todoList", JSON.stringify(todoListArr));

        // Mettre à jour la liste dans l'état Redux
        state.todoList = todoListArr;

        
      }
    },

    updateFilterStatus: (state, action) => {
      state.filterStatus = action.payload
    }
  },
});

// Action creators are generated for each case reducer function
export const { addTodo, deleteTodo,updateTodo,updateFilterStatus } = todoSlice.actions;

export default todoSlice.reducer;
