import "./App.css";
import PageTitle from "./components/PageTitle";
import AppHeader from "./components/AppHeader";
import Grid from "@mui/material/Unstable_Grid2";
import Box from "@mui/material/Box";
import { Container } from "@mui/material";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppContent from "./components/AppContent";

const App = () => {

  


  return (
    <>
      <Container maxWidth="md">
        <ToastContainer />
        <Box sx={{ flexGrow: 1 }}>
          <Grid container justifyContent="center" spacing={2}>
            {/* titre  */}
            <PageTitle>TODOLIST BY LATIF</PageTitle>

            {/* entete de la todo */}
            <AppHeader />
            
              <AppContent />
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default App;
